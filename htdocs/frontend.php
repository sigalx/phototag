<?php

require_once dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'env.php';
require_once YII_ROOT.DIRECTORY_SEPARATOR.'yii.php';

$config = dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.
	'frontend'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'main.php';

Yii::createWebApplication( $config )->run();
