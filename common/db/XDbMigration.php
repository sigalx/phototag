<?php

Yii::import( 'application.db.XDbMigration.schema.mysql.XMysqlSchema' );

class XDbMigration extends CDbMigration
{
	public function __construct()
	{
		if ( $this->dbConnection->driverName == 'sqlite' )
			defined( 'MIGRATE_SQLITE' ) or
			define( 'MIGRATE_SQLITE', true );
		if ( $this->dbConnection->driverName == 'mysql' )
			defined( 'MIGRATE_MYSQL' ) or
			define( 'MIGRATE_MYSQL', true );
	}

	public function createTable( $table, $columns, $options = null )
	{
		if ( !isset( $options ) )
			$options = defined( 'MIGRATE_MYSQL' ) ? 'AUTO_INCREMENT = 100' : '';

		return parent::createTable( $table, $columns, $options );
	}

	public function addForeignKey( $name, $table, $columns, $ref_table, $ref_columns, $delete = 'RESTRICT', $update = 'RESTRICT' )
	{
		if ( !defined( 'MIGRATE_SQLITE' ) )
			return parent::addForeignKey( $name, $table, $columns, $ref_table, $ref_columns, $delete, $update );
	}

}
