<?php

class XActiveRecord extends CActiveRecord
{
	public static function model( $dummy = __CLASS__ )
	{
		return parent::model( get_called_class() );
	}

	public function tableName()
	{
		if ( isset( $this->table_name ) )
			return $this->table_name;

		return lcfirst( get_called_class() );
	}

	public function getSafeAttributes( $names = true )
	{
		return array_intersect_key( parent::getAttributes( $names ),
			array_flip( $this->getSafeAttributeNames() ) );
	}

	protected function afterFind()
	{
		parent::afterFind();

		foreach ($this->attributes as $name => $value)
		{
			if ( !is_numeric( $value ) || !is_string( $value ) )
				continue;

			if ( $name === 'id' ||
				endsWithNoCase( $name, '_id' ) ||
				endsWithNoCase( $name, '_on' ) )
			{
				$this->$name = intval( $value );
			}
		}
	}

	public function findById( $id )
	{
		return $this->findByAttributes( array( 'id' => $id ) );
	}

	public function getError( $attribute = null )
	{
		if ( isset( $attribute ) )
			return parent::getError( $attribute );

		if ( ( $error = $this->errors ) )
		{
			$error = reset( $error );
			$error = reset( $error );

			return $error;
		}

		return null;
	}

	/**
	 * @param array $attributes
	 * @param bool  $safe_only
	 * @param bool  $throw_exception
	 *
	 * @return false|XActiveRecord
	 * @throws CException
	 */
	public static function create( array $attributes, $safe_only = true, $throw_exception = true )
	{
		$model = new static;
		$model->setAttributes( $attributes, $safe_only );

		if ( $model->save() )
			return $model;

		if ( ( $error = $model->errors ) && $throw_exception )
		{
			$error = reset( $error );
			$error = reset( $error );

			throw new CHttpException( 400, Yii::t( 'common', $error ) );
		}

		if ( $throw_exception )
			throw new CException( Yii::t( 'common', 'Cannot create the active record for model {model}', array( '{model}' => get_called_class() ) ) );

		return false;
	}

}
