<?php

class XSqliteSchema extends CSqliteSchema
{
	public $columnTypes = array
	(
		'pk' => 'INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL',
        'bigpk' => 'INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL',
		'string' => 'VARCHAR(255)',
		'text' => 'TEXT',
		'integer' => 'INTEGER',
		'float' => 'FLOAT',
		'decimal' => 'DECIMAL',
		'datetime' => 'DATETIME',
		'timestamp' => 'TIMESTAMP',
		'time' => 'TIME',
		'date' => 'DATE',
		'binary' => 'BLOB',
		'boolean' => 'TINYINT(1)',
		'money' => 'DECIMAL(19,4)',
	);

}
