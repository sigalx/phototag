<?php

class XMysqlSchema extends CMysqlSchema
{
	public $columnTypes = array
	(
		'pk' => 'INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
		'string' => 'VARCHAR(255)',
		'text' => 'TEXT',
		'integer' => 'INT UNSIGNED',
		'float' => 'FLOAT',
		'decimal' => 'DECIMAL',
		'datetime' => 'DATETIME',
		'timestamp' => 'TIMESTAMP',
		'time' => 'TIME',
		'date' => 'DATE',
		'binary' => 'BINARY',
		'boolean' => 'TINYINT(1)',
		'money' => 'DECIMAL(19,4)',
	);

}
