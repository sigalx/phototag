<?php

setlocale( LC_ALL, 'ru_RU.UTF-8' );

require_once dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'common'.DIRECTORY_SEPARATOR.'CommonFunctions.php';

return array
(
	'basePath' => dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..',

	'name' => 'Phototag',

	'sourceLanguage' => 'ru',

	'timeZone' => 'Europe/Moscow',

	'preload' => array(),

	'import' => array
	(
		'application.common.components.*',
		'application.common.components.auth.*',
		'application.common.components.identities.*',
		'application.common.behaviors.*',
		'application.common.models.*',

		'application.common.db.XDbMigration',
		'application.common.db.ar.XActiveRecord',
		'application.common.db.schema.mysql.XMysqlSchema',
		'application.common.db.schema.sqlite.XSqliteSchema',
	),

	'components' => array
	(
		'cache' => array
		(
			'class' => 'CDbCache',
			'autoCreateCacheTable' => YII_DEBUG ? true : false,
		),

		'db' => array
		(
			'class' => 'system.db.CDbConnection',
			'schemaCachingDuration' => YII_DEBUG ? 0 : 2592000, // 30 days

			'connectionString' => DB_CONNECTION_STRING,
			'emulatePrepare' => true,
			'username' => DB_USERNAME,
			'password' => DB_PASSWORD,
			'charset' => 'utf8',

			'driverMap' => array(
				'pgsql' => 'CPgsqlSchema',    // PostgreSQL
				'mysqli' => 'XMysqlSchema',   // MySQL
				'mysql' => 'XMysqlSchema',    // MySQL
				'sqlite' => 'XSqliteSchema',  // sqlite 3
				'sqlite2' => 'XSqliteSchema', // sqlite 2
				'mssql' => 'CMssqlSchema',    // Mssql driver on windows hosts
				'dblib' => 'CMssqlSchema',    // dblib drivers on linux (and maybe others os) hosts
				'sqlsrv' => 'CMssqlSchema',   // Mssql
				'oci' => 'COciSchema',        // Oracle driver
			),
		),

        'user' => array(
            'class' => 'WebUser',
        ),
	),

	'params' => array
	(
		'admin_email' => 'sigalx@sigalx.ru',

		'secret_token' => APP_SECRET_TOKEN,

		'vkapp' => array
		(
			'app_id' => VK_APP_ID,
			'secret' => VK_APP_SECRET,
		),

	),

);
