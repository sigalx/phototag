<?php

class CommonFunctions {}

function beginsWith( $source, $head )
{
	return $head === '' || substr( $source, 0, strlen( $head ) ) === $head;
}

function beginsWithNoCase( $source, $head )
{
	return $head === '' || strtolower( substr( $source, 0, strlen( $head ) ) ) === strtolower( $head );
}

function endsWith( $source, $tail )
{
	return $tail === "" || substr( $source, -strlen( $tail ) ) === $tail;
}

function endsWithNoCase( $source, $tail )
{
	return $tail === "" || strtolower( substr( $source, -strlen( $tail ) ) ) === strtolower( $tail );
}

function cutHead( $source, $head )
{
	if ( beginsWith( $source, $head ) )
	{
		return substr( $source, strlen( $head ) );
	}

	return false;
}

function cutTail( $source, $tail )
{
	if ( endsWith( $source, $tail ) )
	{
		return substr( $source, 0, -strlen( $tail ) );
	}

	return false;
}

function notEmpty( $value )
{
	return (bool) $value;
}

function explodeCommaSeparated( $source, $comma = ',' )
{
	return array_values( array_filter( array_map( 'trim', explode( $comma, $source ) ), 'notEmpty' ) );
}

function unixtimeToDatetime( $unixtime )
{
	return date( 'Y-m-d H:i:s', $unixtime );
}

function combineArray( $keys, $values = null )
{
	if ( !$values )
		$values = $keys;

	return array_combine( $keys, $values );
}

function ipaddrToInt( $ipaddr )
{
	$matches = null;
	if ( !preg_match( '/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/', $ipaddr, $matches ) )
		return false;

	if ( count( $matches ) != 5 )
		return false;

	$result = 0;

	for ( $n = 1; $n < 5; $n++ )
	{
		if ( intval( $matches[ $n ] ) > 255 )
			return false;

		$result += intval( $matches[ $n ] ) * pow( 256, 4 - $n );
	}

	return $result;
}

function println( $message = '' )
{
	print $message.PHP_EOL;
}

function xstrtolower( $source )
{
	return mb_convert_case( $source, MB_CASE_LOWER, 'utf-8' );
}
