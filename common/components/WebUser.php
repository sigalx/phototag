<?php

class WebUser extends CWebUser
{
    /**
     * @return boolean
     * @throws CException
     */
    public function getIsAdmin()
    {
        if ($this->getIsGuest()) {
            return false;
        }
        $user = ARUser::model()->findByUsername($this->getId());
        if (!$user) {
            throw new CException('User model has not been found');
        }
        return boolval($user->is_admin);
    }

}
