<?php

class m140411_185524_tags extends XDbMigration
{
	public function safeUp()
	{
		$this->createTable( 'tags', array
		(
            'id' => 'pk',
			'tag' => 'string NOT NULL',
		) );

		$this->createIndex( 'tags_tag_uniq', 'tags', 'tag', true );

		return true;
	}

	public function safeDown()
	{
		$this->dropTable( 'tags' );

		return true;
	}
}
