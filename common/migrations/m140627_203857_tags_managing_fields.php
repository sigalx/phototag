<?php

class m140627_203857_tags_managing_fields extends XDbMigration
{
	public function safeUp()
	{
		$this->addColumn( 'tags', 'group_name', 'string' );
		$this->addColumn( 'tags', 'group_order', 'integer' );

		return true;
	}

	public function safeDown()
	{
		$this->dropColumn( 'tags', 'group_order' );
		$this->dropColumn( 'tags', 'group_name' );

		return true;
	}
}
