<?php

class m140411_185515_photos extends XDbMigration
{
	public function safeUp()
	{
		$this->createTable( 'photos', array
		(
            'id' => 'pk',
			'uid' => 'CHAR(10) NOT NULL',
			'group_uid' => 'CHAR(10) DEFAULT NULL',
            'title' => 'string DEFAULT NULL',
            'tags' => 'string DEFAULT NULL',
			'uploaded_on' => 'BIGINT UNSIGNED NOT NULL',
			'changed_on' => 'BIGINT UNSIGNED DEFAULT NULL',
			'filename' => 'string NOT NULL',
			'filesize' => 'integer NOT NULL',
			'thumbnail_filename' => 'string NOT NULL',
			'content_sha1' => 'binary(20) NOT NULL',
			'picture_width' => 'integer NOT NULL',
			'picture_height' => 'integer NOT NULL',
		) );

		$this->createIndex( 'photos_uid_uniq', 'photos', 'uid', true );

		$this->createIndex( 'photos_uploaded_on_uniq', 'photos', 'uploaded_on' );
		$this->createIndex( 'photos_changed_on_uniq', 'photos', 'changed_on' );
		$this->createIndex( 'photos_content_sha1_uniq', 'photos', 'content_sha1' );

		return true;
	}

	public function safeDown()
	{
		$this->dropTable( 'photos' );

		return true;
	}
}
