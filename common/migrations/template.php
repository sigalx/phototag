<?php

class {ClassName} extends XDbMigration
{
	public function safeUp()
	{
		$this->createTable( 'test', array
		(
            'id' => 'pk',
            'name' => 'string NOT NULL',
            'age' => 'integer NOT NULL',
		) );

		return true;
	}

	public function safeDown()
	{
		$this->dropTable( 'test' );

		return true;
	}
}
