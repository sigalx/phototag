<?php

class m140411_185637_photos_tags extends XDbMigration
{
	public function safeUp()
	{
		$this->createTable( 'photos_tags', array
		(
            'id' => 'pk',
			'photo_id' => 'integer NOT NULL',
			'tag_id' => 'integer NOT NULL',
		) );

		$this->createIndex( 'photos_tags_photo_id_tag_id_uniq', 'photos_tags', 'photo_id, tag_id', true );

		$this->createIndex( 'photos_tags_photo_id', 'photos_tags', 'photo_id' );
		$this->createIndex( 'photos_tags_tag_id', 'photos_tags', 'tag_id' );

		return true;
	}

	public function safeDown()
	{
		$this->dropTable( 'photos_tags' );

		return true;
	}
}
