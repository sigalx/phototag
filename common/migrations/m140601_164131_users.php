<?php

class m140601_164131_users extends XDbMigration
{
	public function safeUp()
	{
		$this->createTable( 'users', array
		(
            'id' => 'pk',
            'username' => 'string NOT NULL',
			'password' => 'string NOT NULL',
		) );

		$this->insert( 'users', array(
			'username' => 'adm1n',
			'password' => 't43k0aMzg3Ow'
		) );

		return true;
	}

	public function safeDown()
	{
		$this->dropTable( 'users' );

		return true;
	}
}
