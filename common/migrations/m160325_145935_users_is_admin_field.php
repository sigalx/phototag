<?php

class m160325_145935_users_is_admin_field extends XDbMigration
{
    public function safeUp()
    {
        $this->addColumn('users', 'is_admin', 'boolean');
        $this->dbConnection->commandBuilder
            ->createUpdateCommand('users', ['is_admin' => true], new CDbCriteria(['condition' => '1 = 1']))
            ->execute();

        $this->insert('users', [
            'username' => 'family',
            'password' => 'photo',
        ]);

        return true;
    }

    public function safeDown()
    {
        $this->delete('users', 'username = :username', [':username' => 'family']);
        $this->dropColumn('users', 'is_admin');
        return true;
    }
}
