<?php

/**
 * Class ARTag
 * @property integer id
 * @property string tag
 * @property string group_name
 * @property integer group_order
 *
 * @property array photos
 */
class ARTag extends XActiveRecord
{
	public $table_name = 'tags';

	public function rules()
	{
		return CMap::mergeArray( parent::rules(), array
		(
			array( 'tag', 'length', 'max' => 255 ),
			array( 'group_name', 'length', 'max' => 255 ),
			array( 'group_order', 'numerical', 'integerOnly' => true ),

		) );
	}

	public function relations()
	{
		return array
		(
			'photos' => array( self::MANY_MANY, 'ARPhoto', 'photos_tags(tag_id, photo_id)' ),
		);
	}

	public function findAllByTag( $tag )
	{
		return $this->findAll( '`tag` LIKE :tag', array( ':tag' => '%'.xstrtolower( $tag ).'%' ) );
	}

	public function findByTag( $tag )
	{
		return $this->findByAttributes( array( 'tag' => xstrtolower( $tag ) ) );
	}

	public function searchPhotosByTags( $tags )
	{
		$tags = explodeCommaSeparated( $tags, '#' );

		return array_reduce(
			array_reduce( $tags, function( $carry, $item )
			{
                $found = ARTag::model()->with( 'photos' )->findByTag( $item );

                if (!$found) {
                    $found = ARTag::model()->with('photos')->findAllByTag($item);
                } else {
                    $found = [$found];
                }

				foreach ( $found as $t )
					$carry = CMap::mergeArray( $carry, $t->photos );

				return $carry;
			},
			array() ),
			function( $carry, ARPhoto $item ) use ( $tags )
			{
				if ( isset( $carry[ $item[ 'id' ] ] ) )
					return $carry; // already exists

//				$item_tags = '#'.implode( '#', xstrtolower( $tags ) ); //array_map( function( $item ) { return xstrtolower( $item ); }, $item[ 'tags' ] );

				foreach ( $tags as $tag )
					if ( !$item->hasTag( $tag ) )
						return $carry;

				$carry[ $item[ 'id' ] ] = $item;
				return $carry;
			},
			array()
		);
	}

	public function getRatedTags( $limit = 10, $threshold = 5 )
	{
		$result = $this->dbConnection->createCommand()
			->selectDistinct( array( 'tag', 'COUNT( photo_id ) AS frequency' ) )
			->from( 'photos_tags' )
			->leftJoin( 'tags', 'tags.id = photos_tags.tag_id' )
			->having( 'COUNT( photo_id ) > ' . intval( $threshold ) )
			->group( 'tag_id' )
			->limit( $limit )
			->queryAll();

		return $result;
	}

	public function searchTags( $part, $limit = 10 )
	{
		$criteria = new CDbCriteria;
		$criteria->compare( 'tag', $part, true );
		$criteria->limit = $limit;

		$this->setDbCriteria( $criteria );
		return $this->findAll();
	}

	public static function create( array $attributes, $safe_only = true, $throw_exception = true )
	{
		if ( !empty( $attributes['tag'] ) )
			$attributes['tag'] = xstrtolower( $attributes['tag'] );
		return parent::create( $attributes, $safe_only, $throw_exception );
	}

	public static function getTagMetadata()
	{
		return array_map(
			function( ARTag $tag ) { return (object) $tag->attributes; },
			static::model()->findAll() );
	}

	public static function setTagMetadata( array $data )
	{
		foreach ( $data as $attributes )
		{
			$tag = static::model()->findByPk( $attributes->id );

			if ( !$tag )
				$tag = new ARTag;

			$renew_required = false;

			if ($tag->attributes[ 'tag' ] !== $attributes->tag )
				$renew_required = true;

			$tag->attributes = (array) $attributes;
			$tag->save();

			if ( $renew_required )
			{
				$transaction = static::model()->dbConnection->beginTransaction();
				foreach ( $tag->photos as $photo )
				{
					$photo->tags = array_map( function( ARTag $item ) { return $item->tag; }, $photo->related_tags );
					$photo->save();
				}
				$transaction->commit();
			}
		}

		return true;
	}
}
