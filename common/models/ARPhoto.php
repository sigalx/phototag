<?php

/**
 * Class ARPhoto
 * @property integer id
 * @property string uid
 * @property string group_uid
 * @property string title
 * @property string tags
 * @property integer uploaded_on
 * @property integer changed_on
 * @property string filename
 * @property integer filesize
 * @property string thumbnail_filename
 * @property string content_sha1
 * @property integer picture_width
 * @property integer picture_height
 *
 * @property array related_tags
 */
class ARPhoto extends XActiveRecord
{
	public $table_name = 'photos';

//	/**
//	 * @var CUploadedFile
//	 */
//	public $file;

	public function rules()
	{
		return CMap::mergeArray( parent::rules(), array
		(
//			array( 'file', 'file', 'types' => 'jpg, jpeg' ),

			array( 'title', 'length', 'max' => 255 ),

		) );
	}

	public function relations()
	{
		return array
		(
			'related_tags' => array( self::MANY_MANY, 'ARTag', 'photos_tags(photo_id, tag_id)' ),
		);
	}

	public function behaviors()
	{
		return array
		(
			'uid_generator' => array( 'class' => 'UidGeneratorBehavior' ),
		);
	}

	public function beforeSave()
	{
		if ( !parent::beforeSave() )
			return false;

		if ( $this->isNewRecord )
		{
			$this->uid = static::makeUid();
			$this->uploaded_on = time();
			$this->tags = '';
		}

		if ( is_string( $this->tags ) )
			$this->tags = explodeCommaSeparated( $this->tags, '#' );

		if ( is_array( $this->tags ) )
		{
			$this->tags = array_unique( $this->tags );
			$this->tags = !empty( $this->tags ) ? '#'.implode( '#', $this->tags ) : '';
		}

		return true;
	}

	public function afterSave()
	{
		$tags = explodeCommaSeparated( $this->tags, '#' );

		foreach ( $tags as $tag )
		{
			$t = ARTag::model()->findByTag( $tag );
			if ( !$t )
				$t = ARTag::create( array( 'tag' => $tag ) );

			$this->addRelatedTag( $t->id );
		}

		foreach ( $this->related_tags as $t )
			if ( !$this->hasTag( $t->tag ) )
				$this->removeRelatedTag( $t->id );
	}

	public function beforeDelete()
	{
		if ( !parent::beforeDelete() )
			return false;

		$transaction = $this->dbConnection->beginTransaction();

		foreach ( $this->related_tags as $tag )
			$this->removeRelatedTag( $tag->id );

		$transaction->commit();

		return true;
	}

	public function afterFind()
	{
		$this->tags = explodeCommaSeparated( $this->tags, '#' );

		$this->picture_width = intval( $this->picture_width );
		$this->picture_height = intval( $this->picture_height );
	}

	public static function createFromUploadedFile( CUploadedFile $file, $group_uid = null )
	{
		$original_path = Yii::getPathOfAlias( 'application.htdocs.photos.original' );
		$thumbnails_path = Yii::getPathOfAlias( 'application.htdocs.photos.thumbnails' );

		$model = new ARPhoto;

		$model->group_uid = $group_uid;
		$model->content_sha1 = sha1_file( $file->getTempName(), true );
		$model->title = $file->getName();
		$model->filename = sha1( $model->content_sha1.uniqid() );
		$model->filename = substr( $model->filename, 0, 2 ).'/'.substr( $model->filename, 2 ).'.jpg'; //.'_'.$file->getName();
		$model->filesize = $file->getSize();

		$model->thumbnail_filename = sha1( $model->content_sha1.uniqid() );
		$model->thumbnail_filename = substr( $model->thumbnail_filename, 0, 2 ).'/'.substr( $model->thumbnail_filename, 2 ).'.jpg';
		$thumbnail_file_path = $thumbnails_path.DIRECTORY_SEPARATOR.str_replace( '/', DIRECTORY_SEPARATOR, $model->thumbnail_filename );
		if ( !file_exists( dirname( $thumbnail_file_path ) ) )
			mkdir( dirname( $thumbnail_file_path ) );
		static::saveThumbnail( $file->getTempName(), $thumbnail_file_path );

		$image_size = getimagesize( $file->getTempName() );
		$model->picture_width = intval( $image_size[ 0 ] );
		$model->picture_height = intval( $image_size[ 1 ] );

		$full_file_path = $original_path.DIRECTORY_SEPARATOR.str_replace( '/', DIRECTORY_SEPARATOR, $model->filename );

		if ( ( !file_exists( dirname( $full_file_path ) ) && !mkdir( dirname( $full_file_path ) ) ) ||
			!$file->saveAs( $full_file_path ) ||
			!$model->save()
		)
		{
			throw new CHttpException( 500, 'Не удалось сохранить загруженный файл' );
		}
	}

	protected static function saveThumbnail( $source, $target )
	{
		Yii::import( 'application.common.lib.SimpleImage' );

		$image = new SimpleImage();
		$image->load( $source );
		if ( $image->getWidth() * 360 > $image->getHeight() * 480 )
			$image->resizeToWidth( 480 );
		else
			$image->resizeToHeight( 360 );
		$image->save( $target );
	}

	/**
	 * @param integer $tag_id
	 * @return bool
	 */
	protected function addRelatedTag( $tag_id )
	{
		if ( $this->hasRelatedTag( $tag_id ) )
			return false;

		return Yii::app()->db->createCommand()->insert( 'photos_tags', array(
			'photo_id' => $this->id,
			'tag_id' => $tag_id
		) ) > 0;
	}

	/**
	 * @param integer $tag_id
	 * @return bool
	 */
	protected function removeRelatedTag( $tag_id )
	{
		if ( !$this->hasRelatedTag( $tag_id ) )
			return false;

		return Yii::app()->db->createCommand()->delete( 'photos_tags', 'photo_id = :photo_id AND tag_id = :tag_id', array(
			':photo_id' => $this->id,
			':tag_id' => $tag_id
		) ) > 0;
	}

	/**
	 * @param integer $tag_id
	 * @return bool
	 */
	protected function hasRelatedTag( $tag_id )
	{
		return Yii::app()->db->createCommand()->
			select( 'id' )->
			from( 'photos_tags' )->
			where( 'photo_id = :photo_id AND tag_id = :tag_id', array(
				':photo_id' => $this->id,
				':tag_id' => $tag_id ) )->
			queryScalar() !== false;
	}

	/**
	 * @param string $tag
	 * @return bool
	 */
	public function hasTag( $tag )
	{
		$tags = $this->tags;
		if ( is_array( $tags ) )
			$tags = !empty( $tags ) ? '#'.implode( '#', $tags ) : '';
		return strpos( xstrtolower( $tags ), xstrtolower( $tag ) ) !== false;
	}

	public function addTags( $tags, array $attributes )
	{
		$models = $this->findAllByAttributes( $attributes );

		foreach ( $models as $model )
		{
			$model->tags = '#'.implode( '#', $model->tags ).'#'.$tags;
			$model->save();
		}

		return count( $models );
	}

	public function addTitle( $title )
	{
		$this->title = $title;
		return $this->save();
	}

	/**
	 * @param array|string $tags
	 * @param array        $attributes
	 *
	 * @return integer
	 */
	public function updateTags( $tags, array $attributes )
	{
		$models = static::model()->findAllByAttributes( $attributes );

		foreach ( $models as $model )
		{
			$model->tags = $tags;
			$model->save();
		}

		return count( $models );
	}

}
