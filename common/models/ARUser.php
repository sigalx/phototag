<?php

/**
 * Class ARUser
 * @property integer id
 * @property string username
 * @property string password
 * @property boolean is_admin
 */
class ARUser extends XActiveRecord
{
	public $table_name = 'users';

	public function rules()
	{
		return CMap::mergeArray( parent::rules(), array() );
	}

	public function relations()
	{
		return array();
	}

    public function findByUsername($value)
    {
        return $this->findByAttributes([
            'username' => $value,
        ]);
    }

}
