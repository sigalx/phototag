<?php

class UidGeneratorBehavior extends CBehavior
{
	public static function makeUid()
	{
		return substr( base64_encode( sha1( uniqid() ) ), 0, 10);
	}

}
