<?php

$common_config = require dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'common'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'main.php';

return CMap::mergeArray( $common_config, array
(
	'runtimePath' => dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'runtime',

	'language' => 'en',

	'commandPath' => dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'commands',

	'commandMap' => array
	(
		'migrate' => array(
			'class' => 'system.cli.commands.MigrateCommand',
			'migrationPath' => 'application.common.migrations',
			'templateFile' => 'application.common.migrations.template',
			'interactive' => false,
		),

	),

	'preload' => array( 'log' ),

	'import' => array
	(
		'application.console.components.*',
	),

	'components' => array
	(),

	'params' => array
	()

) );
