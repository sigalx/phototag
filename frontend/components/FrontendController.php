<?php

class FrontendController extends CController
{
	public $layout = '//layouts/main';
	public $breadcrumbs = array();
	public $pageTitle = '';

	public function init()
	{
		parent::init();

		$this->breadcrumbs = array(
			$this->pageTitle => array( 'index' ),
		);
	}

	public function filters()
	{
		return array( 'accessControl' );
	}

	public function accessRules()
	{
		return [
            parent::accessRules(),
			['deny'],
		];
	}

	public function getViewPath()
	{
		$module = null;
		if ( ( $module = $this->getModule() ) === null )
			$module = Yii::app();

		return $module->getViewPath().DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.$this->getId();
	}

	protected function beforeAction( $action )
	{
		if ( !parent::beforeAction( $action ) )
			return false;

		if ( $this->action->id === 'index' && endsWith( Yii::app()->request->pathInfo, 'index' ) )
			$this->redirect( cutTail( Yii::app()->createUrl( $this->id.'/'.$this->action->id ), 'index' ) );

		return true;
	}

	protected function beforeRender( $view )
	{
		if ( !parent::beforeRender( $view ) )
			return false;

		if ( empty( $this->pageTitle ) )
			$this->pageTitle = ucfirst( basename( $this->action->id ) );

		return true;
	}
}

;
