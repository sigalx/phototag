<?php

class FrontendAssetManager extends CAssetManager
{
	public function publish($path, $hash_by_name = false, $level = -1, $force_copy = null)
	{
		if ( !file_exists( $path ) )
			$path = Yii::getPathOfAlias( 'application.frontend.assets' ).DIRECTORY_SEPARATOR.$path;

		return parent::publish($path, $hash_by_name, $level, $force_copy);
	}
};
