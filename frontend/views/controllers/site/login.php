<div class="form">
	<?php $form = $this->beginWidget( 'CActiveForm', array
	(
		'id' => 'login-form',
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'htmlOptions' => array
		(
			'action' => 'site/login',
			'enctype' => 'multipart/form-data',
			'role' => 'form',
		)

	) ); ?>

	<?php echo $form->errorSummary( $model ); ?>

	<fieldset style="margin-top: 30px; margin-bottom: 30px;">
		<div class="form-group">
			<?php echo $form->label( $model, 'username' ); ?>
			<?php echo $form->textField( $model, 'username', array( 'class' => 'form-control' ) ); ?>
			<?php echo $form->error( $model, 'username' ); ?>
		</div>
		<div class="form-group">
			<?php echo $form->label( $model, 'password' ); ?>
			<?php echo $form->passwordField( $model, 'password', array( 'class' => 'form-control' ) ); ?>
			<?php echo $form->error( $model, 'password' ); ?>
		</div>
	</fieldset>

	<div>
		<?php echo CHtml::submitButton( 'Войти', array( 'class' => 'btn btn-primary btn-lg' ) ); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>
