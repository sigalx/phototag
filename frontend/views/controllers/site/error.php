<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = '';
$this->breadcrumbs = false;
?>

<div class="error-message">
<?php echo nl2br(CHtml::encode($message)); ?>
</div>
