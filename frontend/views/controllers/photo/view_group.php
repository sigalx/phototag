<?php //$this->widget( 'EditableTags', array( 'tags' => '', 'ajax_url' => array( 'photo/changeTag', 'group_uid' => $group_uid ) ) ); ?>
<?php $this->widget( 'TagSearch', array( 'tags' => isset( $tags ) ? $tags : '' ) ); ?>

<?php if ( Yii::app()->user->isAdmin ): ?>
<div class="sidebar"><div class="add-tag-button inline-block glyphicon glyphicon-plus btn btn-primary btn-lg"></div></div>
<?php endif; ?>

<?php
/** @var CPagination $pages */

$photos_on_page = count( $photos );
/** @var CClientScript $cs */
$cs = Yii::app()->clientScript;
$cs->registerScript('all-photos', sprintf('var all_photos = %s;', json_encode($all_photos)), CClientScript::POS_BEGIN);
$cs->registerScript('all-photos-count', sprintf('var all_photos_count = %d;', count($all_photos)), CClientScript::POS_BEGIN);
$cs->registerScript('current-photo-number', 'var current_photo_number = null;', CClientScript::POS_BEGIN);
?>

<div class="info-header">
	<span>Найдено <?php echo isset( $pages ) ? $pages->itemCount : $photos_on_page; ?> фотографий</span>
	<?php if ( Yii::app()->user->isAdmin ): ?>
		<a class="select-all-link">Выделить все</a>
		<a class="select-none-link">Снять выделение</a>
	<?php endif; ?>
</div>

<div class="add-tag-dialog display-none">
<?php
echo CHtml::tag( 'div', array( 'class' => 'add-tag-dialog-form' ),
	CHtml::beginForm( array( 'photo/addTag' ) ).
	CHtml::tag( 'div', array(),
		CHtml::textField( 'tags', '', array( 'class' => 'add-tag-dialog-input tag-autocomplete', 'placeholder' => 'Введите теги' ) ).
		CHtml::tag( 'div', array( 'class' => 'add-tag-dialog-submit inline-block glyphicon glyphicon-ok btn btn-primary btn-lg' ), '' ).
//		CHtml::ajaxSubmitButton( 'Сохранить', array( 'photo/addTag', 'group_uid' => $group_uid ), array( 'success' => 'function() { window.location.reload(); }' ), array( 'class' => 'hidden' ) )
		CHtml::submitButton( 'Сохранить', array( 'class' => 'hidden' ) ).
		CHtml::tag( 'div', array( 'class' => 'select-tags-button glyphicon glyphicon-th-list btn btn-primary btn-lg' ), '' )
	).
	CHtml::endForm()
);
?>
</div>

<div class="add-title-dialog display-none">
	<?php
	echo CHtml::tag( 'div', array( 'class' => 'add-title-dialog-form' ),
		CHtml::beginForm( array( 'photo/addTitle' ) ).
		CHtml::tag( 'div', array(),
			CHtml::textField( 'title', '', array( 'class' => 'add-title-dialog-input', 'placeholder' => 'Введите описание' ) ).
			CHtml::tag( 'div', array( 'class' => 'add-title-dialog-submit inline-block glyphicon glyphicon-ok btn btn-primary btn-lg' ), '' ).
//		CHtml::ajaxSubmitButton( 'Сохранить', array( 'photo/addTitle', 'group_uid' => $group_uid ), array( 'success' => 'function() { window.location.reload(); }' ), array( 'class' => 'hidden' ) )
			CHtml::submitButton( 'Сохранить', array( 'class' => 'hidden' ) )
		).
		CHtml::endForm()
	);
	?>
</div>

<div class="full-photo-dialog display-none">
	<div class="full-photo-dialog-img"></div>
	<div class="full-photo-dialog-title"></div>
	<div class="full-photo-dialog-tags"></div>
	<div class="full-photo-dialog-controls">
		<a class="inline-block glyphicon glyphicon-chevron-left full-photo-dialog-prev"></a>
		<a class="inline-block glyphicon glyphicon-chevron-right full-photo-dialog-next"></a>
	</div>
</div>

<div style="margin-bottom: 20px"></div>

<?php
	$paging_url = array( $this->id . '/' . $this->action->id );
	foreach ( $_GET as $key => $value )
		$paging_url[ $key ] = $value;
	unset( $paging_url[ 'page_size' ] );
	unset( $paging_url[ 'page' ] );
?>

<?php if ( isset( $pages ) ): ?>
	<div>Показывать по:
		<?php echo CHtml::link( '10', CMap::mergeArray( $paging_url, array( 'page_size' => 10, 'page' => 1 ) ) ); ?> |
		<?php echo CHtml::link( '20', CMap::mergeArray( $paging_url, array( 'page_size' => 20, 'page' => 1 ) ) ); ?> |
		<?php echo CHtml::link( '50', CMap::mergeArray( $paging_url, array( 'page_size' => 50, 'page' => 1 ) ) ); ?> |
        <?php echo CHtml::link( '100', CMap::mergeArray( $paging_url, array( 'page_size' => 100, 'page' => 1 ) ) ); ?> |
        <?php echo CHtml::link( '500', CMap::mergeArray( $paging_url, array( 'page_size' => 500, 'page' => 1 ) ) ); ?>
	</div>
	<div>
		<?php
		echo CHtml::beginForm( $paging_url, 'GET' );
		echo CHtml::tag( 'span', array(), 'Перейти на страницу: ' );
		echo CHtml::hiddenField( 'page_size', $pages->pageSize );
		echo CHtml::textField( 'page', '', array( 'style' => 'width: 50px' ) );
		echo CHtml::submitButton( 'Go', array( 'name' => null ) );
		echo CHtml::endForm();
		?>
	</div>
<?php endif; ?>

<?php if ( isset( $pages ) ): ?>
	<div class="link-pager-block">
		<?php $this->widget( 'CLinkPager', array(
			'htmlOptions' => array(
				'class' => 'link-pager',
			),
			'header' => 'Страницы: ',
			'firstPageLabel' => 'первая',
			'lastPageLabel' => 'последняя',
			'nextPageLabel' => 'вперед',
			'prevPageLabel' => 'назад',
			'pages' => $pages

		) ); ?>
	</div>
<?php endif; ?>

<div class="photo-thumbnails <?php echo !Yii::app()->user->isAdmin ? '' : 'selectable-photo-thumbnails'; ?>" data-total-photos="<?php echo $photos_on_page; ?>">
<?php for ( $current_photo_number = 0; $current_photo_number < $photos_on_page; $current_photo_number++ ):
	$photo = $photos[ $current_photo_number ]; ?>
	<?php
		$max_width = 360;

		if ( $photo->picture_width * 360 < $photo->picture_height * 480 )
			$max_width = round( $photo->picture_width * 220 / $photo->picture_height + 60 );
	?>
	<div class="photo-thumbnail" data-photo-uid="<?php echo $photo->uid; ?>" style="max-width: <?php echo $max_width.'px'; ?>;">
		<div class="photo-thumbnail-links">
			<div style="inline-block">
			<?php echo CHtml::link( '', array( 'photo/f/'.$photo->uid ), array(
				'class' => 'open-photo-link glyphicon glyphicon-zoom-in',
				'data-current-number' => $pages->pageSize * $pages->currentPage + $current_photo_number,
			) ); ?>
			<?php echo !Yii::app()->user->isAdmin ? '' : CHtml::link( '', '', array(
				'class' => 'add-title-link glyphicon glyphicon-font',
			) ); ?>
			<?php echo !Yii::app()->user->isAdmin ? '' : CHtml::link( '', '', array(
				'class' => 'delete-photo-link glyphicon glyphicon-trash',
			) ); ?>
			</div>
		</div>
		<div class="photo-thumbnail-image selectable-item">
			<?php echo CHtml::image(
				Yii::app()->getBaseUrl().'/photos/thumbnails/'.$photo->thumbnail_filename,
				$photo->title ? $photo->title : 'Без названия',
				array(
					'class' => !Yii::app()->user->isAdmin ? 'open-photo-link cursor-pointer' : '',
                    'data-current-number' => $pages->pageSize * $pages->currentPage + $current_photo_number,
				) ); ?>
		</div>
		<?php echo CHtml::tag( 'div', array( 'class' => 'photo-thumbnail-title' ), $photo->title ); ?>
		<?php if ( Yii::app()->user->isAdmin ) { ?>
			<?php $this->widget( 'EditableTags', array( 'tags' => !empty( $photo->tags ) ? '#'.implode( ' #', $photo->tags ) : '', 'ajax_url' => array( 'photo/changeTag', 'uids' => $photo->uid ) ) ); ?>
		<?php } else { ?>
			<?php echo CHtml::tag( 'div', array( 'class' => 'photo-thumbnail-tags' ), '#'.implode( ' #', $photo->tags ) ); ?>
		<?php } ?>
	</div>
<?php endfor; ?>
</div>

<div style="margin-bottom: 20px"></div>

<?php if ( isset( $pages ) ): ?>
	<div>Показывать по:
		<?php echo CHtml::link( '10', CMap::mergeArray( $paging_url, array( 'page_size' => 10, 'page' => 1 ) ) ); ?> |
		<?php echo CHtml::link( '20', CMap::mergeArray( $paging_url, array( 'page_size' => 20, 'page' => 1 ) ) ); ?> |
		<?php echo CHtml::link( '50', CMap::mergeArray( $paging_url, array( 'page_size' => 50, 'page' => 1 ) ) ); ?> |
        <?php echo CHtml::link( '100', CMap::mergeArray( $paging_url, array( 'page_size' => 100, 'page' => 1 ) ) ); ?> |
        <?php echo CHtml::link( '500', CMap::mergeArray( $paging_url, array( 'page_size' => 500, 'page' => 1 ) ) ); ?>
	</div>
	<div>
		<?php
		echo CHtml::beginForm( $paging_url, 'GET' );
		echo CHtml::tag( 'span', array(), 'Перейти на страницу: ' );
		echo CHtml::hiddenField( 'page_size', $pages->pageSize );
		echo CHtml::textField( 'page', '', array( 'style' => 'width: 50px' ) );
		echo CHtml::submitButton( 'Go', array( 'name' => null ) );
		echo CHtml::endForm();
		?>
	</div>
<?php endif; ?>

<?php if ( isset( $pages ) ): ?>
	<div class="link-pager-block">
		<?php $this->widget( 'CLinkPager', array(
			'htmlOptions' => array(
				'class' => 'link-pager',
			),
			'header' => 'Страницы: ',
			'firstPageLabel' => 'первая',
			'lastPageLabel' => 'последняя',
			'nextPageLabel' => 'вперед',
			'prevPageLabel' => 'назад',
			'pages' => $pages

		) ); ?>
	</div>
<?php endif; ?>
