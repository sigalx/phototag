<?php $this->widget( 'TagSearch' ); ?>

<div class="tag-cloud">
	<?php
	shuffle($rated_tags);
	foreach ( $rated_tags as $item )
	{
		$font_size = round( $item[ 'frequency' ] / $max_frequency * 40 + 10 );
		echo CHtml::link( '#'.$item[ 'tag' ].PHP_EOL, array( 'photo/search', 'tags' => $item[ 'tag' ] ), array( 'class' => 'tag-cloud-item', 'style' => 'font-size: '.$font_size.'px' ) );
	}
	?>
</div>
