<div class="form">
	<?php $form = $this->beginWidget( 'CActiveForm', array
	(
		'enableAjaxValidation' => true,
		'enableClientValidation' => true,
		'htmlOptions' => array
		(
			'action' => 'photo/upload',
			'enctype' => 'multipart/form-data'
		)

	) ); ?>

	<?php echo $form->errorSummary( $model ); ?>

	<fieldset style="margin-top: 30px; margin-bottom: 30px;">
		<div><?php echo $form->fileField( $model, 'files[]', array( 'multiple' => 'multiple' ) ); ?></div>
	</fieldset>

	<div>
		<?php echo CHtml::submitButton( 'Загрузить фотографии', array( 'class' => 'btn btn-primary btn-lg' ) ); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>
