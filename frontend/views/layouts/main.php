<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="language" content="ru" />

	<title><?php echo Yii::app()->name.( $this->pageTitle ? ' — '.CHtml::encode( $this->pageTitle ) : '' ); ?></title>

	<?php
    /** @var CClientScript $cs */
	$cs = Yii::app()->clientScript;
    /** @var FrontendAssetManager $am */
    $am = Yii::app()->assetManager;

	$cs->registerPackage( 'jquery.ui' );
	$cs->registerPackage( 'bootstrap' );

    $cs->registerScript('global-base-url', sprintf('var global_base_url = \'%s\';',
        Yii::app()->urlManager->showScriptName ? Yii::app()->request->hostInfo . Yii::app()->request->scriptUrl : Yii::app()->getBaseUrl(true)), CClientScript::POS_BEGIN);

	$cs->registerScriptFile( $am->publish( 'scripts/common.js' ) );
	$cs->registerCssFile( $am->publish( 'styles/common.css' ) );
	?>

</head>
<body>

<div class="select-tags-dialog display-none">
	<form></form>
</div>

<div class="jumbotron">
	<div class="container">
		<h1 class="header-logo"><?php echo CHtml::link( '#phototag', array( 'photo/index' ) ); ?></h1>

		<div class="admin-panel">
			<?php if ( !Yii::app()->user->isGuest ): ?>
				<?php echo CHtml::tag( 'span', array(), Yii::app()->user->name.' ('.CHtml::link( 'выйти', array( 'site/logout' ) ).')' ); ?>
                <?php if ( Yii::app()->user->isAdmin ): ?>
				    <?php echo CHtml::link( 'Загрузить фото', array( 'photo/upload' ) ); ?>
                    <?php echo CHtml::link( 'Все добавленные', array( 'photo/all' ) ); ?>
                    <?php echo CHtml::link( 'Управление тегами', array( 'photo/manageTags' ) ); ?>
                <?php endif; ?>
			<?php else: ?>
				<?php echo CHtml::link( 'Войти', array( 'site/login' ) ); ?>
			<?php endif; ?>
		</div>

		<?php echo $content; ?>
	</div>
</div>

</body>
</html>
