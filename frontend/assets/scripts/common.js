var editable_tags_hide_on_focusleave = true;

function showEditableTagsForm()
{
	$( '.editable-tags-form' ).addClass( 'hidden' );
	$( '.editable-tags-label' ).removeClass( 'hidden' );

	var label = $( this );
	var form = $( '.editable-tags-form', $( this ).parent() );
	var input = $( '.editable-tags-form-input', form );
	label.addClass( 'hidden' );
	if ( !label.hasClass( 'no-tags' ) )
		input.val( label.text() );
	form.removeClass( 'hidden' );
	input.focus();
	input[ 0 ].setSelectionRange( input.val().length, input.val().length );
}

function hideEditableTagsForm()
{
	if ( editable_tags_hide_on_focusleave )
	{
		$( this ).parents( '.editable-tags-form' ).addClass( 'hidden' );
		$( '.editable-tags-label', $( this ).parents( '.editable-tags' ) ).removeClass( 'hidden' );
	}
}

function onSuccessSubmitEditableTags( widget )
{
	var form = $( '.editable-tags-form', $( '#' + widget ) );
	form.addClass( 'hidden' );
	var label = $( '.editable-tags-label', $( '#' + widget ) );
	var tags = $( '.editable-tags-form-input', form ).val();
	if ( tags.length )
	{
		label.removeClass( 'no-tags' );
		tags = '#' + tags.
			split( '#' ).
			map(function ( $item ) { return $.trim( $item ); } ).
			filter(function ( item ) { return item.length > 0; } ).
			filter(function ( item, index, self ) { return self.indexOf( item ) === index; } ).
			join( ' #' );
		label.text( tags );
	}
	else
	{
		label.addClass( 'no-tags' );
		label.text( 'Без тегов' );
	}
	label.removeClass( 'hidden' );
}

function onSearchResponse()
{

}

function initFullPhotoDialog()
{
    var params = all_photos[current_photo_number];
	var max_width = $( window ).width() * 0.8;
	var max_height = $( window ).height() * 0.8;
	var img_width = params.img_width;
	var img_height = params.img_height;

	if ( img_width / max_width < img_height / max_height )
		max_width = Math.floor( img_width / img_height * max_height );
	else
		max_height = Math.floor( img_height / img_width * max_width );

	$( '.full-photo-dialog .full-photo-dialog-img' ).html(
		'<img src="' + params.img_src + '" alt=""' +
			'style="max-width: ' + ( max_width ) + 'px; max-height: ' + ( max_height - 150 ) + 'px"/>'
	);

	$( '.full-photo-dialog .full-photo-dialog-title' ).html( params.title ? params.title : 'Без описания' );
	$( '.full-photo-dialog .full-photo-dialog-tags' ).html( params.tags ? params.tags : 'Без тегов' );

	$( '.full-photo-dialog' )
		.attr( 'data-photo-uid', params.uid )
		.dialog( 'option', 'title', 'Фотография ' + ( current_photo_number + 1 ) + ' из ' + all_photos_count )
		.dialog( 'option', 'width', max_width + 50 )
		.dialog( 'option', 'height', max_height + 50 )
		.dialog( 'open' );

	max_height += $( '.full-photo-dialog .full-photo-dialog-title' ).height();
	max_height += $( '.full-photo-dialog .full-photo-dialog-tags' ).height();
	max_height += $( '.full-photo-dialog .full-photo-dialog-controls' ).height();

	$( '.full-photo-dialog' )
		.dialog( 'option', 'height', max_height - 50 )
}

function initSelectTagsDialog( callback )
{
	$( '.select-tags-dialog > form' ).html( '<div class="accordion"></div>' );
	var ajax_url = global_base_url + '/photo/getAllTags';
	$.ajax( ajax_url, {
		type:    'GET',
		success: function ( tags )
		{
			var group_name = undefined;

			for ( var n in tags )
			{
				if ( group_name !== tags[ n ].group_name )
				{
					group_name = tags[ n ].group_name;
					if ( group_name === null )
						$( '.select-tags-dialog > form > div.accordion' ).append( '<h3>Без группы</h3><div></div>' );
					else
						$( '.select-tags-dialog > form > div.accordion' ).append( '<h3>' + group_name + '</h3><div></div>' );
				}

				$( '.select-tags-dialog > form > div.accordion > div:last-child' ).append( '<div><input id="select-tags-dialog-input-' + n + '" type="checkbox" value="' + tags[ n ].tag + '" />' +
						'<label for="select-tags-dialog-input-' + n + '">' + tags[ n ].tag + '</label></div>' );
			}
			$( '.select-tags-dialog > form > div.accordion' ).accordion();
			$( '.select-tags-dialog > form > div.accordion' ).accordion( 'option', 'heightStyle', 'auto' );
			$( '.select-tags-dialog > form' ).append( '<input type="submit" value="Выбрать теги" />');
			$( '.select-tags-dialog > form input[type="submit"]' ).click( callback );
			$( '.select-tags-dialog' ).dialog( 'open' );
		}
	} );
}

function init()
{
	$( '.selectable-photo-thumbnails' ).selectable( {
		filter: '.selectable-item',
		cancel: '.editable-tags, .photo-thumbnail-links'
//		delay: 200
	} );

	$( '.select-tags-dialog' ).dialog( {
		autoOpen:  false,
		modal:     true,
		draggable: false,
		width:    500
	} );
	$( '.tag-search-form .select-tags-button' ).click( function ()
	{
		initSelectTagsDialog( function()
		{
			var search_url = global_base_url + '/photo/search?tags=';
			$( '.select-tags-dialog > form input:checked' )
				.each( function( index ) { search_url += ( index > 0 ? '+%23' : '%23' ) + $( this ).attr( 'value' ); } );
			$( '.select-tags-dialog > form' ).attr( 'method', 'GET' ).attr( 'action', search_url );
			window.location.href = search_url;
			return false;
		} );
	} );
	$( '.editable-tags-form .select-tags-button' ).click( function ()
	{
		editable_tags_hide_on_focusleave = true;
		initSelectTagsDialog( function( event )
		{
			var input_val = '';
			$( '.select-tags-dialog > form input:checked' )
				.each( function( index ) { input_val += ( index > 0 ? ' #' : '#' ) + $( this ).attr( 'value' ); } );
			$( '.editable-tags-form .editable-tags-form-input' ).val( input_val );
			event.preventDefault();
			editable_tags_hide_on_focusleave = true;
			$( '.select-tags-dialog' ).dialog( 'close' );
			return false;
		} );
	} ).
		mouseenter( function () { editable_tags_hide_on_focusleave = false; } ).
		mouseleave( function () { editable_tags_hide_on_focusleave = true; } );
	$( '.add-tag-dialog-form .select-tags-button' ).click( function ()
	{
		initSelectTagsDialog( function( event )
		{
			var input_val = '';
			$( '.select-tags-dialog > form input:checked' )
				.each( function( index ) { input_val += ( index > 0 ? ' #' : '#' ) + $( this ).attr( 'value' ); } );
			$( '.add-tag-dialog-form .add-tag-dialog-input' ).val( input_val );
			event.preventDefault();
			$( '.select-tags-dialog' ).dialog( 'close' );
			return false;
		} );
	} );

	$( '.add-tag-dialog' ).dialog( {
		autoOpen:  false,
		modal:     true,
		draggable: false,
		width:     350,
		title:     'Добавить теги'
	} );
	$( '.add-tag-button' ).click( function ()
	{
		$( '.add-tag-dialog' ).dialog( 'open' );
	} );

	$( '.add-title-dialog' ).dialog( {
		autoOpen:  false,
		modal:     true,
		draggable: false,
		title:     'Редактировать описание'
	} );
	$( '.add-title-link' ).click( function ()
	{
		var photo_uid = $( this ).parents( '.photo-thumbnail' ).attr( 'data-photo-uid' );
		var action_url = global_base_url + '/photo/addTitle?uid=' + photo_uid;
		$( '.add-title-dialog-form > form' ).attr( 'action', action_url );
		var title = $( '.photo-thumbnail-title', $( this ).parents( '.photo-thumbnail' ) ).text();
		$( '.add-title-dialog-form .add-title-dialog-input' ).val( title );
		$( '.add-title-dialog' ).dialog( 'open' );
	} );

	$( '.full-photo-dialog' ).dialog( {
		autoOpen:  false,
		modal:     true,
		draggable: false
	} );
	$( '.open-photo-link' ).click( function ()
	{
        current_photo_number = parseInt($( this ).attr( 'data-current-number' ));
		initFullPhotoDialog();
		return false;
	} );
	$( '.full-photo-dialog-prev' ).click( function ()
	{
        $( '.full-photo-dialog' ).dialog( 'close' );
        current_photo_number -= 1;
        if (current_photo_number < 0) {
            current_photo_number = all_photos_count - 1;
        }
        initFullPhotoDialog();
	} );
	$( '.full-photo-dialog-next' ).click( function ()
	{
		$( '.full-photo-dialog' ).dialog( 'close' );
        current_photo_number += 1;
        if (current_photo_number >= all_photos_count) {
            current_photo_number = 0;
        }
        initFullPhotoDialog();
	} );

	$( '.editable-tags-label' ).click( showEditableTagsForm );
	$( '.editable-tags-form-input' ).focusout( hideEditableTagsForm );
	$( '.editable-tags-form-submit' ).click(function ()
	{
		$( 'input[type="submit"]', $( this ).parents( '.editable-tags-form' ) ).click();
	} ).
		mouseenter( function () { editable_tags_hide_on_focusleave = false; } ).
		mouseleave( function () { editable_tags_hide_on_focusleave = true; } );
	$( '.editable-tags-form' ).submit( function ()
	{
		$( 'input[type="submit"]', $( this ) ).click();
		return false;
	} );

	$( '.tag-search-form-submit' ).click( function ()
	{
		$( 'input[type="submit"]', $( this ).parents( '.tag-search-form' ) ).click();
	} );

	$( '.add-tag-dialog-form' ).submit( function ()
	{
		$( 'input[type="submit"]', $( this ) ).click();
		return false;
	} );
	$( '.add-tag-dialog-submit' ).click( function ()
	{
		$( 'input[type="submit"]', $( this ).parents( '.add-tag-dialog' ) ).click();
	} );
	$( '.add-tag-dialog-form input[type="submit"]' ).click( function ()
	{
		var uids = [];
		$( '.selectable-photo-thumbnails .selectable-item.ui-selected' ).each( function ( index, elem )
		{
			uids.push( $( elem ).parents( '.photo-thumbnail' ).attr( 'data-photo-uid' ) );
		} );
		var tags = $( '.add-tag-dialog-form .add-tag-dialog-input' ).val();
		var ajax_url = $( '.add-tag-dialog-form > form' ).attr( 'action' );
		$.ajax( ajax_url + '?uids=' + uids, {
			type:    'POST',
			data:    { tags: tags },
			success: function () { window.location.reload(); }
		} );
		return false;
	} );

	$( '.add-title-dialog-form' ).submit( function ()
	{
		$( 'input[type="submit"]', $( this ) ).click();
		return false;
	} );
	$( '.add-title-dialog-submit' ).click( function ()
	{
		$( 'input[type="submit"]', $( this ).parents( '.add-title-dialog' ) ).click();
	} );
	$( '.add-title-dialog-form input[type="submit"]' ).click( function ()
	{
		var title = $( '.add-title-dialog-form .add-title-dialog-input' ).val();
		var ajax_url = $( '.add-title-dialog-form > form' ).attr( 'action' );
		$.ajax( ajax_url, {
			type:    'POST',
			data:    { title: title },
			success: function () { window.location.reload(); }
		} );
		return false;
	} );

	$( '.select-all-link' ).click( function ()
	{
		$( '.selectable-photo-thumbnails .selectable-item' ).each( function ( index, elem )
		{
			$( elem ).addClass( 'ui-selected' );
		} );
	} );
	$( '.select-none-link' ).click( function ()
	{
		$( '.selectable-photo-thumbnails .selectable-item' ).each( function ( index, elem )
		{
			$( elem ).removeClass( 'ui-selected' );
		} );
	} );

	$( '.tag-autocomplete' ).autocomplete( { source: function( request, response )
	{
		var part = request.term;
		var entered = part.split( '#' );
		entered = entered.filter( function( value ) { return value.length; } );
		part = entered[ entered.length - 1 ];
		part = part.trim();
		entered.pop();
		entered = entered.length ? '#' + entered.join( '#' ) : '';

		var ajax_url = global_base_url + '/photo/searchTags';
		$.ajax( ajax_url, {
			type:    'GET',
			data:    { part: part },
			success: function ( ajax_response )
			{
				var suggessted = ajax_response;
				for ( var n in suggessted )
					suggessted[ n ] = entered + '#' + suggessted[ n ];
				response( suggessted );
			}
		} );

	} } );

	$( '.delete-photo-link' ).click( function()
	{
		if ( !confirm( 'Действительно хотите удалить данную фотографию?' ) )
			return;

		var photo_uid = $( this ).parents( '.photo-thumbnail' ).attr( 'data-photo-uid' );
		var ajax_url = global_base_url + '/photo/delete?uid=' + photo_uid;
		$.ajax( ajax_url, {
			type:    'POST',
			success: function ()
			{
				window.location.reload();
			}
		} );

	} );

}

$( init );
