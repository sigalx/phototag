<?php

$common_config = require dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'common'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'main.php';

return CMap::mergeArray( $common_config, array
(
	'runtimePath' => dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'runtime',

	'language' => 'ru',

	'defaultController' => 'photo',
	'controllerPath' => dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'controllers',
	'viewPath' => dirname( __FILE__ ).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'views',

	'preload' => array( 'log' ),

	'import' => array
	(
		'application.frontend.components.*',
		'application.frontend.models.*',
		'application.frontend.web.helpers.*',
		'application.frontend.web.widgets.*',
	),

	'components' => array
	(
		'assetManager' => array
		(
			'class' => 'FrontendAssetManager',
			'basePath' => ASSET_PATH,
		),

		'clientScript' => array
		(
			'scriptMap' => array
			(
				'jquery.js' => '//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.js',
				'jquery.min.js' => '//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js',

				'jquery-ui.js' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.js',
				'jquery-ui.min.js' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js',

				'jquery-ui.css' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css',
				'jquery-ui.min.css' => '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css',

				'bootstrap.js' => '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.js',
				'bootstrap.min.js' => '//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js',

				'bootstrap.css' => '//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css',
				'bootstrap.min.css' => '//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css',
			),
			'packages' => array
			(
				'jquery.ui' => array
				(
					'js' => array( YII_DEBUG ? 'jquery-ui.js' : 'jquery-ui.min.js' ),
					'css' => array( YII_DEBUG ? 'jquery-ui.css' : 'jquery-ui.min.css' ),
					'depends' => array( 'jquery' )
				),
				'bootstrap' => array
				(
					'js' => array( YII_DEBUG ? 'bootstrap.js' : 'bootstrap.min.js' ),
					'css' => array( YII_DEBUG ? 'bootstrap.css' : 'bootstrap.min.css' ),
					'depends' => array( 'jquery' )
				),
			),
		),

		'urlManager' => array
		(
			'urlFormat' => 'path',
			'showScriptName' => YII_DEBUG,
			'caseSensitive' => true,
			'useStrictParsing' => true,

			'rules' => array
			(
				array( 'photo', 'pattern' => '' ),
				array( '<controller>', 'pattern' => '<controller:\w+>' ),
				array( '<controller>/<action>', 'pattern' => '<controller:\w+>/<action:\w+>' ),
				array( '<controller>/<action>', 'pattern' => '<controller:\w+>/<action:\w+>/<id:\d+>' ),
				array( '<controller>/<action>', 'pattern' => '<controller:\w+>/<action:\w+>/<uid:\w+>' ),
			)
		),

		'user' => array
		(
			'allowAutoLogin' => true
		),

		'session' => array
		(
			'class' => 'CDbHttpSession',
			'autoStart' => true,
			'cookieMode' => 'allow'
		),

		'errorHandler' => array
		(
			'errorAction' => 'site/error'
		),
	),

	'params' => array
	(
	)

) );
