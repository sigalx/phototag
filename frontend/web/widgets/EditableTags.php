<?php

class EditableTags extends XWidget
{
	public $ajax_url;
	public $tags = '';

	public function run()
	{
		$form =
			CHtml::tag( 'div', array( 'class' => 'hidden editable-tags-form' ),
				CHtml::beginForm().
				CHtml::tag( 'div', array(),
					CHtml::textField( 'tags', $this->tags, array( 'class' => 'editable-tags-form-input tag-autocomplete', 'placeholder' => 'Без тегов', 'id' => $this->id.'-editable-tags-form-input' ) ).
					CHtml::tag( 'div', array( 'class' => 'editable-tags-form-submit inline-block glyphicon glyphicon-ok btn btn-primary btn-lg' ), '' ).
					CHtml::ajaxSubmitButton( 'Сохранить', $this->ajax_url, array( 'success' => 'function() { onSuccessSubmitEditableTags( \''.$this->id.'\'); }' ), array( 'class' => 'hidden' ) ).
					CHtml::tag( 'div', array( 'class' => 'select-tags-button inline-block glyphicon glyphicon-th-list btn btn-primary btn-lg' ), '' )
				).
				CHtml::endForm()
			);

		$label =
			CHtml::tag( 'div', array( 'class' => 'editable-tags-label'.( $this->tags ? '' : ' no-tags' ) ), $this->tags ? $this->tags : 'Без тегов' );

		echo CHtml::tag( 'div', array( 'id' => $this->id, 'class' => 'editable-tags' ), $form.$label );
	}

}
