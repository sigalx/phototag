<?php

class TagSearch extends XWidget
{
	public $tags = '';

	public function run()
	{
		echo CHtml::tag( 'div', array( 'class' => 'tag-search-form' ),
			CHtml::beginForm( array( 'photo/search' ), 'GET' ).
			CHtml::tag( 'div', array(),
				CHtml::textField( 'tags', $this->tags, array( 'class' => 'tag-search-form-input tag-autocomplete', 'placeholder' => 'Поиск по тегам' ) ).
				CHtml::tag( 'div', array( 'class' => 'tag-search-form-submit inline-block glyphicon glyphicon-search btn btn-primary btn-lg' ), '' ).
				CHtml::submitButton( 'Поиск', array( 'class' => 'hidden', 'name' => '' ) ).
				CHtml::tag( 'div', array( 'class' => 'select-tags-button glyphicon glyphicon-th-list btn btn-primary btn-lg' ), '' )
			).
			CHtml::endForm()
		);
	}

}
