<?php

class SiteController extends FrontendController
{
	public $pageTitle = 'Welcome to Phototag';

	public function init()
	{
		parent::init();

		$this->breadcrumbs = array(
			$this->pageTitle => array( 'index' ),
		);
	}

	public function accessRules()
	{
		return array_merge(
			array(
				array( 'allow',
					/*'actions' => array('error')*/ ),
			),
			parent::accessRules()
		);
	}

	public function actionIndex()
	{
		$this->render( 'index' );
	}

	public function actionLogin()
	{
		$model = new LoginForm;

		if ( Yii::app()->request->isAjaxRequest )
		{
			echo CActiveForm::validate( $model );
			Yii::app()->end();
		}

		if ( Yii::app()->request->isPostRequest )
		{
			$model->attributes = $_POST[ 'LoginForm' ];

			if ( $model->validate() && $model->login() )
				$this->redirect( Yii::app()->user->returnUrl );
		}

		$this->render( 'login', array( 'model' => $model ) );
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect( array( Yii::app()->defaultController.'/index' ) );
	}

	public function actionError()
	{
		if ( $error = Yii::app()->errorHandler->error )
		{
			if ( Yii::app()->request->isAjaxRequest )
				echo $error[ 'message' ]; // TODO
			else
				$this->render( 'error', $error );
		}
	}
}

;
