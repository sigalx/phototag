<?php

class PhotoController extends FrontendController
{
	public $pageTitle = 'Phototag';

    public function accessRules()
    {
        return array_merge([
            [
                'deny',
                'actions' => ['upload', 'addTitle', 'delete', 'addTag', 'changeTag', 'all'],
                'users' => ['@'],
                'expression' => function (WebUser $user, CAccessRule $rule) {
                    return !$user->getIsAdmin();
                }
            ],
            [
                'allow',
                'users' => ['@'],
            ],
        ],
            parent::accessRules()
        );
    }

	public function actionIndex()
	{
		$rated_tags = ARTag::model()->getRatedTags( -1 );

		$max_frequency = 0;
		array_walk( $rated_tags, function( &$item ) use ( &$max_frequency )
			{
				if ( $item[ 'frequency' ] > 500 )
					$item[ 'frequency' ] = 500;
				if ( $max_frequency < $item[ 'frequency' ] )
					$max_frequency = $item[ 'frequency' ];
			}
		);

		$this->render( 'index', array( 'model' => new ARPhoto, 'rated_tags' => $rated_tags, 'max_frequency' => $max_frequency ) );
	}

	public function actionF( $uid )
	{
		$photo = ARPhoto::model()->findByAttributes( array(
			'uid' => $uid
		) );

		if ( !$photo ) {
			throw new CHttpException( 404, 'Запись не найдена' );
		}

		$this->render( 'view_full', array( 'photo' => $photo ) );
	}

	public function actionGroup( $uid, $page_size = 20 )
	{
		$pages = new CPagination( ARPhoto::model()->countByAttributes( array(
			'group_uid' => $uid
		) ) );
		$pages->pageSize = intval( $page_size );

        $all_photos = ARPhoto::model()->findAllByAttributes( array(
			'group_uid' => $uid
		) );

		$photos = array_slice( $all_photos, $pages->offset, $pages->limit );

        $all_photos = array_values(array_map(function (ARPhoto $item) {
            return [
                'uid' => $item->uid,
                'title' => $item->title,
                'tags' => $item->tags ? '#'.implode( ' #', $item->tags ) : '',
                'img_src' => Yii::app()->getBaseUrl().'/photos/original/'.$item->filename,
                'img_width' => $item->picture_width,
                'img_height' => $item->picture_height,
            ];
        }, $all_photos));

		$this->render( 'view_group', array( 'all_photos' => $all_photos, 'photos' => $photos, 'group_uid' => $uid, 'pages' => $pages ) );
	}

	public function actionSearch( $tags )
	{
		$all_photos = ARTag::model()->searchPhotosByTags( $tags );

		$pages = new CPagination( count( $all_photos ) );
		$pages->pageSize = intval( Yii::app()->request->getParam( 'page_size', 20 ) );

		$photos = array_slice( $all_photos, $pages->offset, $pages->limit );

        $all_photos = array_values(array_map(function (ARPhoto $item) {
            return [
                'uid' => $item->uid,
                'title' => $item->title,
                'tags' => $item->tags ? '#'.implode( ' #', $item->tags ) : '',
                'img_src' => Yii::app()->getBaseUrl().'/photos/original/'.$item->filename,
                'img_width' => $item->picture_width,
                'img_height' => $item->picture_height,
            ];
        }, $all_photos));

		$this->render( 'view_group', array( 'all_photos' => $all_photos, 'photos' => $photos, 'tags' => $tags, 'pages' => $pages ) );
	}

	public function actionAll( $page_size = 20 )
	{
		$pages = new CPagination( ARPhoto::model()->count() );
		$pages->pageSize = intval( $page_size );

        $all_photos = ARPhoto::model()->findAll();

        $photos = array_slice( $all_photos, $pages->offset, $pages->limit );

        $all_photos = array_values(array_map(function (ARPhoto $item) {
            return [
                'uid' => $item->uid,
                'title' => $item->title,
                'tags' => $item->tags ? '#'.implode( ' #', $item->tags ) : '',
                'img_src' => Yii::app()->getBaseUrl().'/photos/original/'.$item->filename,
                'img_width' => $item->picture_width,
                'img_height' => $item->picture_height,
            ];
        }, $all_photos));

		$this->render( 'view_group', array( 'all_photos' => $all_photos, 'photos' => $photos, 'pages' => $pages ) );
	}

	public function actionManageTags()
	{
		if ( Yii::app()->request->isPostRequest && Yii::app()->request->getParam( 'type' ) === 'download' )
		{
			$content = json_encode( ARTag::getTagMetadata(), JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE );

			header( 'Content-Type: application/octet-stream' );
			header( 'Content-Description: File Transfer' );
			header( 'Content-Disposition: attachment; filename=phototag-tag-metadata-'.date( 'Y-m-d-H-i-s' ).'.txt' );
			header( 'Content-Transfer-Encoding: binary' );
			header( 'Expires: 0' );
			header( 'Cache-Control: must-revalidate' );
			header( 'Pragma: public' );
			header( 'Content-Length: ' . strlen( $content ) );

			print( $content );
			Yii::app()->end();
		}

		if ( Yii::app()->request->isPostRequest && Yii::app()->request->getParam( 'type' ) === 'upload' )
		{
			$file = CUploadedFile::getInstanceByName( 'metadata' );

			if ( !$file )
				throw new CHttpException( 400, 'Wheare iz de file?' );

			$data = json_decode( file_get_contents( CUploadedFile::getInstanceByName( 'metadata' )->getTempName() ) );

			if ( !isset( $data ) || gettype( $data ) !== 'array' )
				throw new CHttpException( 400, 'Bad JSON' );

			ARTag::setTagMetadata( $data );
			$this->redirect( array( 'photo/index' ) );
		}

		$this->render( 'manage_tags' );
	}

	// AJAX actions

	public function actionGetAllTags()
	{
		if ( !Yii::app()->request->isAjaxRequest )
			return $this->redirect( array( 'photo/index' ) );

		$criteria = new CDbCriteria;
		$criteria->order = 'group_name, group_order';

		$tags = ARTag::model()->findAll( $criteria );
		$tags = array_reduce( $tags, function( $carry, ARTag $tag ) { $carry[] = $tag->attributes; return $carry; }, array() );

		header( 'Content-Type: application/json' );
		print( CJSON::encode( $tags ) );
		Yii::app()->end();
	}

	public function actionSearchTags()
	{
		if ( !Yii::app()->request->isAjaxRequest )
			return $this->redirect( array( 'photo/index' ) );

		$part = Yii::app()->request->getParam( 'part' );

		if ( !isset( $part ) )
			throw new CHttpException( 400, 'Отсутствуют обязательные параметры' );

		$tags = ARTag::model()->searchTags( $part );
		$tags = array_reduce( $tags, function( $carry, $item ) { $carry[] = $item[ 'tag' ]; return $carry; }, array() );

		header( 'Content-Type: application/json' );
		print( CJSON::encode( $tags ) );
		Yii::app()->end();
	}

	public function actionAddTag( $uids )
	{
		if ( !Yii::app()->request->isAjaxRequest )
			return $this->redirect( array( 'photo/index' ) );

		$uids = explodeCommaSeparated( $uids );
		$tags = Yii::app()->request->getPost( 'tags' );

		if ( !isset( $tags ) )
			throw new CHttpException( 400, 'Отсутствуют обязательные параметры' );

		$affected = ARPhoto::model()->addTags( $tags, array(
			'uid' => $uids
		) );

		println( sprintf( 'Применено к %d записям', $affected ) );
		Yii::app()->end();
	}

	public function actionDelete( $uid )
	{
		if ( !Yii::app()->request->isAjaxRequest )
			return $this->redirect( array( 'photo/index' ) );

		$photo = ARPhoto::model()->findByAttributes( array(
			'uid' => $uid
		) );

		if ( !$photo ) {
			throw new CHttpException( 404, 'Запись не найдена' );
		}

		$affected = $photo->delete();

		println( sprintf( 'Применено: %s', $affected ? 'успешно' : 'неудача' ) );
		Yii::app()->end();
	}

	public function actionAddTitle( $uid )
	{
		if ( !Yii::app()->request->isAjaxRequest )
			return $this->redirect( array( 'photo/index' ) );

		$title = Yii::app()->request->getPost( 'title' );

		if ( !isset( $title ) )
			throw new CHttpException( 400, 'Отсутствуют обязательные параметры' );

		$photo = ARPhoto::model()->findByAttributes( array(
			'uid' => $uid
		) );

		if ( !$photo ) {
			throw new CHttpException( 404, 'Запись не найдена' );
		}

		$affected = $photo->addTitle( $title );

		println( sprintf( 'Применено: %s', $affected ? 'успешно' : 'неудача' ) );
		Yii::app()->end();
	}

	public function actionChangeTag( $uids )
	{
		if ( !Yii::app()->request->isAjaxRequest )
			return $this->redirect( array( 'photo/index' ) );

		$uids = explodeCommaSeparated( $uids );
		$tags = Yii::app()->request->getPost( 'tags' );

		if ( !isset( $tags ) )
			throw new CHttpException( 400, 'Отсутствуют обязательные параметры' );

		$affected = ARPhoto::model()->updateTags( $tags, array(
			'uid' => $uids
		) );

		println( sprintf( 'Применено к %d записям', $affected ) );
		Yii::app()->end();
	}

	// upload

	public function actionUpload()
	{
		if ( Yii::app()->request->isPostRequest )
		{
			$files = CUploadedFile::getInstances( ARPhoto::model(), 'files' );

			$group_uid = UidGeneratorBehavior::makeUid();

			foreach ( $files as $file )
				ARPhoto::createFromUploadedFile( $file, $group_uid );

			$this->redirect( array( 'photo/group', 'uid' => $group_uid ) );
		}

		$this->render( 'upload', array( 'model' => new ARPhoto ) );
	}
}

;
